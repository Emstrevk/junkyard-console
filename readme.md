# Junkyard Console
A collection of projects aiming to create a seamless couch gaming flow for barebones linux systems.

# General outline (probably not 100% implemented):
- You plug in your controller
- The launcher screen starts and shows all your installed games
- Backend handles wether to use wine etc.
- Support for using gamepad as mouse, configuring game source folders etc.
- Network support for uploading games to a source folder from other network computers
- gd-launcher/res/executors.json outlines game launching parameters 

# Tech stack (tryna keep this up to date):
- godot engine (3.2) for the launcher (exporting is iffy)
- autorandr postswitch script for docked mode detection (If you wish to have "plugged into tv" behaviour)
- xboxdrv for mapping controller output to mouse when needed
	- antimicro fallback in case xboxdrv never goes anywhere 
