#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

echo $SCRIPTPATH

if ps -eaf | grep -v grep | grep gd-launcher | grep -v $$
then
	notify-send "gd-launcher is already running"
else
	# Add -f at the end for fullscreen configuration
	(cd ${SCRIPTPATH}/gd-launcher && godot -f)
fi
