#!/usr/bin/env python3

# Script from the internet
# Listens to udev events (in this case usb device events) to launch the given args
# We want to instea expand to something like this:
# https://bitbucket.org/denilsonsa/small_scripts/src/default/auto_configure_upon_usb_device_change.py

from pyudev import Context, Monitor
import subprocess
import sys

def main():
    context = Context()
    print(context)
    monitor = Monitor.from_netlink(context=context)
    print(monitor)

    monitor.filter_by(subsystem='usb')

    monitor.start()
    subprocess.call(['notify-send', 'controller_detect.py initialized'])

    for device in iter(monitor.poll, None):
        # I can add more logic here, to run different scripts for different devices

        action_device_tuple = monitor.receive_device()
        action = action_device_tuple[0]

        if device.device_type == "usb_device":
            if action == "add":
                print("Usb device added")
                subprocess.call(['notify-send', 'controller_detect.py detected usb, starting gd-launcer'])
                subprocess.Popen('bash ' + sys.path[0] + '/gd-launcher.sh', shell=True)
            elif action == "remove":
                print("Usb device removed")
                subprocess.call(['notify-send', 'controller_detect.py shutting down helper scripts...'])
		subprocess.Popen('bash ' + sys.path[0] + '/cleanup/cleanup.sh', shell=True)

        #print(monitor.receive_device()[0])
        #print(device.device_type)
        

if __name__ == '__main__':
    main()
