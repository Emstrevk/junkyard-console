# Xboxdrv profiles and helper scripts
- Mainly to help use non-xbox controllers as mouse cursor and other fun stuff

# Run xboxdrv without sudo
As dictated by synrgy87 (<3) on Steam forums:
	1. in /etc/udev/rules.d/ make a new file: "55-permissions-uinput.rules"
		- name is unimportant apart from .rules
	2. add to file: KERNEL=="uinput", MODE:="0660", GROUP="input"
	3. This allows users in "input" group to access uinput
	4. Ensure you are in the "input" group in /etc/group 
	5. Also ensure that /etc/modules-load.d/modules.conf has...
		- joydev and uinput under "list of devices loaded at boot"
