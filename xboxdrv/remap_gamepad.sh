#!/bin/bash

CONTROLLER_NAME="Logitech"
CONFIG_FILE="profiles/logitech710-mouse.ini"

EVENT=$(cat /proc/bus/input/devices | grep "$CONTROLLER_NAME" -A 10 | grep -o '\b\w*\event.* \b')
CONTROLLER="/dev/input/"$EVENT

xboxdrv --config "$CONFIG_FILE" --evdev=$CONTROLLER
#sudo xboxdrv --config empty.ini --evdev=$CONTROLLER --alt-config xboxdrv-mouse.ini --evdev=$CONTROLLER --evdev-debug
