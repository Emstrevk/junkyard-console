extends Node2D

# Wrapper for OS.execute() 

# Should be able to take a yml stirng with vars to make an execute call

#
# - executor
# 		runner: wine
#		args:
#			- <FILE_PATH>
#		

var PATH_PLACEHOLDER = "<FILE_PATH>"
var RUNNER_PLACEHOLDER = "<RUNNER>"

var title = ""
var runner = ""
var file_path = ""
var game_folder = ""
var name_filter = ""
var args = []

func from_json_entry(parts : Dictionary):
	title = parts["title"]
	runner = parts["runner"]
	args = parts["args"]
	game_folder = parts["game_folder"]
	
	if "name_filter" in parts:
		name_filter = parts["name_filter"]
		
func run(p_file_path : String):
	
	print("Running executor")
	
	file_path = p_file_path
	
	var parsed_args = []
	for arg in args:
		var pure_arg = _de_placehold(arg)
		#print(pure_arg)
		parsed_args.append(pure_arg)
	
	var output = []
	var arg_debug = ""
	for arg in parsed_args:
		arg_debug += " " + arg
	
	print ("Executing: " + runner + " " + arg_debug)
	OS.execute(_de_placehold(runner), parsed_args, false, output)
	print("Output: " + output[0])
	
	
func _de_placehold(string : String) -> String:
	return string.replace(PATH_PLACEHOLDER, file_path).replace(RUNNER_PLACEHOLDER, runner)
