extends TextureButton

class_name GameButton

var game_path
var title

var executor

signal activated

func set_name(new_name):
	$Label.text = new_name
	title = new_name
	
func get_name():
	return $Label.text
	
func activate():
	print("Button pressed: " + $Label.text)
	emit_signal("activated", self)
	
