extends Node2D

# Core launcher logic for listing and iterating buttons

# TODO: Maybe pager handling is a bit high-level
onready var pager_scene = preload("res://src/Pager.tscn")
onready var button_scene = preload("res://src/Button.tscn")

var buttons = []
var button_index = 0
var selected_button

var button_offset = 0
var selector_draw_offset = Vector2(- 35, + 15)

var offset_increment = 100
var pager

func _ready():
	pager = pager_scene.instance()
	add_child(pager)
	clear()

func _cycle_buttons(forward : bool) -> Node2D:
	if (forward):
		if (button_index < len(buttons) -1):
			button_index += 1
		else:
			button_index = 0
	else:
		if (button_index > 0):
			button_index -= 1
		else:
			button_index = len(buttons) -1
	
	return buttons[button_index]
	
func create_button(name : String) -> Node2D:
	button_offset += offset_increment
	var button = button_scene.instance()
	add_child(button)
	button.rect_position = Vector2(get_viewport_rect().size.x / 2, position.y + button_offset)
	buttons.append(button)
	button.set_name(name)
	
	# Maybe not the best way to do it
	selected_button = _cycle_buttons(true)
	
	_update_pager()
	
	return button
	
func select_next(forward : bool):
	selected_button = _cycle_buttons(forward)
	_update_pager()
	
func choose():
	print("Attempting to press: " + selected_button.title)
	selected_button.activate()
	
# Wipe the button list, hiding the pager
func clear():
	
	button_offset = 0
	button_index = 0
	
	pager.hide()
	
	for button in buttons:
		remove_child(button)
		button.queue_free()
		
	buttons = []
	
func _update_pager():
	var selPos = selected_button.rect_position
	var new_post = selPos + selector_draw_offset
	pager.position = new_post
	pager.show()