extends Node2D

var executors = []
onready var executor_scene = preload("res://src/Executor.tscn")

var lock_input = false

func _input(ev):
	
	if !lock_input:
		if Input.is_action_pressed("ui_down"):
			$ButtonList.select_next(true)
		elif Input.is_action_pressed("ui_up"):
			$ButtonList.select_next(false)
		elif Input.is_action_pressed("ui_accept"):
			print("Selected a button with ENTER")
			release_controls()
			#yield(get_tree().create_timer(0.5),"timeout") # Yield so we don't get stuck in release
			$ButtonList.choose()
	
func release_controls():
		# Immediate release; don't get stuck in "Pressed" when switching focus
	for setting in ProjectSettings.get_property_list():
		if setting.name.substr(0,6) == "input/":
			Input.action_release(setting.name.substr(6, setting.name.length()-6))
			
func executor_screen():
	$ButtonList.clear()
	
	$ScreenTitle.text = "Choose executor"
	
	for exc in executors:
		yield(get_tree().create_timer(0.05), "timeout")
		var button = $ButtonList.create_button(exc.title)
		button.connect("activated", self, "executor_chosen", [exc]) # THis actually works even with no args specified in signal def
		
	var exit_button = $ButtonList.create_button("EXIT")
	exit_button.connect("activated", self, "exit")
	
# Make buttons be available games
# Scan hardcoded dir for game names
# Connect buttons to "game function": "button_activated"
func game_select_screen(executor) -> void:
	
	$ScreenTitle.text = "Choose game to run with " + executor.title
	
	$ButtonList.clear()
	
	var detected_games = scan_dir("games", "")
	
	if (detected_games != null):
		
		var game_list = scan_dir(executor.game_folder, executor.name_filter)
		
		if game_list != null:
			for file in game_list:
				yield(get_tree().create_timer(0.1), "timeout")
				var last_slash = file.find_last("/")
				var button = $ButtonList.create_button(file.substr(last_slash + 1, len(file)))
				button.connect("activated", self, "game_selected", [executor])
				button.game_path = file
				print("Listing game: " + file)
		
	var exit_button = $ButtonList.create_button("BACK")
	exit_button.connect("activated", self, "back_button")
	
	#for arg in executor.args:
	#	print(arg)
	
	print("Game select screen loaded successfully")
	

func _ready():

	yield(get_tree().create_timer(0.1), "timeout")
	parse_executors()
	executor_screen()


# go through the executors json and create object references
func parse_executors() -> void:
	
	var json_dict = {}
	
	var file = File.new()
	file.open("res://res/executors.json", file.READ)
	var text = file.get_as_text()
	
	print(text)
	
	var parsing_data = JSON.parse(text)
	
	json_dict = parsing_data.result
	file.close()
	
	#print(json_dict["executors"][1])
	#print(parsing_data.error_string)
	
	for exe_dict in json_dict["executors"]:
		var new_executor = executor_scene.instance()
		new_executor.from_json_entry(exe_dict)
		executors.append(new_executor)
		#print(new_executor.title)
	
# Takes button because it's brought in with the singal
func exit(button):
	get_tree().quit()
	
func back_button(button):
	executor_screen()
	
func executor_chosen(button, executor):
	print("Chosen executor: " + executor.title)
	game_select_screen(executor)
	
func game_selected(button, executor):
	lock_input = true
	print("Attempting to run " + button.game_path + " with executor " + executor.title)
	executor.run(button.game_path)
	#executor_screen() # Return to executors next time user sees window
	exit(0)

func scan_dir(path, filter_string):
	var file_name
	var files = []
	var dir = Directory.new()
	var error = dir.open(path)
	if error!=OK:
		print("Can't open "+path+"!")
		return
	dir.list_dir_begin(true)
	file_name = dir.get_next()
	while file_name!="":
		if dir.current_is_dir():
			var new_path = path+"/"+file_name
			#print("Found directory "+new_path+".")
			files += scan_dir(new_path, filter_string)
		else:
			var name = path+"/"+file_name
			#print(name)
			if filter_string in file_name or filter_string == "":
				files.push_back(name)
		file_name = dir.get_next()
	dir.list_dir_end()
	return files
