# Tricky to use since subprocess is a bit nasty
from inputs import get_gamepad, get_key, devices, InputEvent
from pymouse import PyMouse
import subprocess

LEFT_JOY_PRESS="BTN_BASE5"
RIGHT_JOY_PRESS="BTN_BASE6"
SELECT="BTN_BASE3"
STATE_DOWN=1
STATE_UP=0

def toggle_mouse_control(current):
    print("Called mouse control toggle")
    #mouse = PyMouse()
    #mouse.move(mouse.position()[0] + 20, mouse.position()[1] + 20)
    if (current == None):
        return subprocess.call('antimicro --profile antimicro-profiles/antimicro-logitech-dualshock-mouse.gamecontroller.amgp', shell=True)
    else:
        current.kill()
        return None

#print("Finished launching toggle script")

mouse_control_process=None

while 1:
    #print("Scanning for gamepad events")
    #devices.gamepads[0].set_vibration(1, 1, 2000)
    events = get_gamepad()
    for event in events:
        if (event.state == STATE_DOWN):
            if (event.code == LEFT_JOY_PRESS): mouse_control_process = toggle_mouse_control(mouse_control_process) 
            if (event.code == RIGHT_JOY_PRESS): mouse_control_process = toggle_mouse_control(mouse_control_process)
            #if (event.code == SELECT): toggle_mouse_control()

