#!/bin/bash

# Script to be used by autorandr for autobooting junkyard on tv connection
# Should be linked into $HOME/.autorandr/postswitch.sh to be autoloaded

# Fetch using first word in (current) tagged configuration
CURRENT_SETUP=$(autorandr | grep "current" | cut -d " " -f1)

notify-send "Autorandr: detected saved display configuration: "$CURRENT_SETUP
